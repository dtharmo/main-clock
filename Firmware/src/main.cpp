#include <Arduino.h>
#include <TimerOne.h>
#include <MsTimer2.h>
#include <ClickEncoder.h>
#include <TM1637Display.h>
#include <MIDI.h>

// DISPLAY
#define TM1637_CLK 2
#define TM1637_IO 3
#define BLINK_TIME 12
volatile int beatLedCount = 0;
TM1637Display display(TM1637_CLK, TM1637_IO);

// ROTARY ENCODER
#define ENC_A A1
#define ENC_B A2
#define ENC_SW A0
#define ENC_STEPS 4
ClickEncoder *encoder;

// INTERNAL CLOCK
#define CLOCKS_PER_BEAT 24
#define MINIMUM_BPM 40
#define MAXIMUM_BPM 300
int bpm;
int lastBpm = 0;
long intervalMicroSeconds;
boolean midiIsPlaying = false;

// CLOCKS
#define CLK_WHOLE_PIN 8
#define CLK_HALF_PIN 7
#define CLK_QUARTER_PIN 6
#define CLK_EIGHTH_PIN 5
#define CLK_SIXTEENTH_PIN 4
#define WHOLE_PULSE 48
#define HALF_PULSE 24
#define QUARTER_PULSE 12
#define EIGHTH_PULSE 6
#define SIXTEENTH_PULSE 3
#define WHOLE_NOTE 96
#define HALF_NOTE 48
#define QUARTER_NOTE 24
#define EIGHTH_NOTE 12
#define SIXTEENTH_NOTE 6
byte wholeNoteCount = 0;
byte halfNoteCount = 0;
byte quarterNoteCount = 0;
byte eighthNoteCount = 0;
byte sixteenthNoteCount = 0;

// MIDI
MIDI_CREATE_DEFAULT_INSTANCE();


void encoderInterrupt() {
  encoder->service();
}

void updateBpm() {
  if (bpm != lastBpm) {
    lastBpm = bpm;
    long interval = 60L * 1000 * 1000 / bpm / CLOCKS_PER_BEAT;
    Timer1.setPeriod(interval);
    display.showNumberDec(bpm, false, 3, 1);
  }
}

void sendPulse(byte clockCount, byte pulseLength, byte clockPin) {
  if (clockCount == 1) digitalWrite(clockPin, HIGH);
  if (clockCount > pulseLength) digitalWrite(clockPin, LOW);
}

void sendClockPulse() {
  MIDI.sendClock();
  beatLedCount++;
  if (midiIsPlaying) {
    wholeNoteCount ++;
    halfNoteCount ++;
    quarterNoteCount ++;
    eighthNoteCount ++;
    sixteenthNoteCount ++;

    sendPulse(wholeNoteCount, WHOLE_PULSE, CLK_WHOLE_PIN);
    sendPulse(halfNoteCount, HALF_PULSE, CLK_HALF_PIN);
    sendPulse(quarterNoteCount, QUARTER_PULSE, CLK_QUARTER_PIN);
    sendPulse(eighthNoteCount, EIGHTH_PULSE, CLK_EIGHTH_PIN);
    sendPulse(sixteenthNoteCount, SIXTEENTH_PULSE, CLK_SIXTEENTH_PIN);

    if (wholeNoteCount == WHOLE_NOTE) wholeNoteCount = 0;
    if (halfNoteCount == HALF_NOTE) halfNoteCount = 0;
    if (quarterNoteCount == QUARTER_NOTE) quarterNoteCount = 0;
    if (eighthNoteCount == EIGHTH_NOTE) eighthNoteCount = 0;
    if (sixteenthNoteCount == SIXTEENTH_NOTE) sixteenthNoteCount = 0;
  }
}

void setup() {
  MIDI.begin(MIDI_CHANNEL_OMNI);
  MIDI.turnThruOff();

  pinMode(CLK_WHOLE_PIN, OUTPUT);
  digitalWrite(CLK_WHOLE_PIN, LOW);
  pinMode(CLK_HALF_PIN, OUTPUT);
  digitalWrite(CLK_HALF_PIN, LOW);
  pinMode(CLK_QUARTER_PIN, OUTPUT);
  digitalWrite(CLK_QUARTER_PIN, LOW);
  pinMode(CLK_EIGHTH_PIN, OUTPUT);
  digitalWrite(CLK_EIGHTH_PIN, LOW);
  pinMode(CLK_SIXTEENTH_PIN, OUTPUT);
  digitalWrite(CLK_SIXTEENTH_PIN, LOW);

  if (bpm < MINIMUM_BPM) bpm = MINIMUM_BPM;
  if (bpm > MAXIMUM_BPM) bpm = MAXIMUM_BPM;

  display.setBrightness(1);
  uint8_t data[] = { 0x00, 0x00, 0x00, 0x00 };
  display.setSegments(data);
  display.showNumberDec(bpm, false, 3, 1);

  encoder = new ClickEncoder(ENC_A, ENC_B, ENC_SW, ENC_STEPS);
  MsTimer2::set(1, encoderInterrupt);
  MsTimer2::start();

  Timer1.initialize(intervalMicroSeconds);
  Timer1.setPeriod(60L * 1000 * 1000 / bpm / CLOCKS_PER_BEAT);
  Timer1.attachInterrupt(sendClockPulse);
}

void loop() {
  // Rotary encoder change bpm
  int value = encoder->getValue();
  if (value != 0) {
    bpm += value;
    if (bpm < MINIMUM_BPM) bpm = MINIMUM_BPM;
    if (bpm > MAXIMUM_BPM) bpm = MAXIMUM_BPM;
    updateBpm();
  }

  // Rotary encoder start/stop
  if (encoder->getButton() == ClickEncoder::Clicked) {
    midiIsPlaying = !midiIsPlaying;
    if (midiIsPlaying) {
      MIDI.sendStart();
      beatLedCount = 0;
    }
    else {
      MIDI.sendStop();
    }
  }

  // Display status
  uint8_t data[1] = { 0x00 };
  if (midiIsPlaying) data[0] = data[0] | SEG_D;
  if (beatLedCount >= CLOCKS_PER_BEAT) beatLedCount = 0;
  if (beatLedCount <= BLINK_TIME) data[0] = data[0] | SEG_A;
  display.setSegments(data, 1, 0);
}

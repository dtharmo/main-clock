EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MainClock-rescue:Arduino_Nano_Every-MCU_Module A1
U 1 1 61A60852
P 9600 2200
F 0 "A1" H 9600 1111 50  0000 C CNN
F 1 "Arduino_Nano_Every" H 9600 1020 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 9600 2200 50  0001 C CIN
F 3 "https://content.arduino.cc/assets/NANOEveryV3.0_sch.pdf" H 9600 2200 50  0001 C CNN
	1    9600 2200
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:TL074-Amplifier_Operational U1
U 2 1 61A64832
P 2500 2950
F 0 "U1" H 2500 3317 50  0000 C CNN
F 1 "TL074" H 2500 3226 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 2450 3050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 2550 3150 50  0001 C CNN
	2    2500 2950
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:TL074-Amplifier_Operational U1
U 3 1 61A65690
P 2500 4700
F 0 "U1" H 2500 5067 50  0000 C CNN
F 1 "TL074" H 2500 4976 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 2450 4800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 2550 4900 50  0001 C CNN
	3    2500 4700
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:TL074-Amplifier_Operational U1
U 4 1 61A6644E
P 2500 6450
F 0 "U1" H 2500 6817 50  0000 C CNN
F 1 "TL074" H 2500 6726 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 2450 6550 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 2550 6650 50  0001 C CNN
	4    2500 6450
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:TL074-Amplifier_Operational U1
U 5 1 61A674DD
P 5100 6650
F 0 "U1" H 5058 6696 50  0000 L CNN
F 1 "TL074" H 5058 6605 50  0000 L CNN
F 2 "" H 5050 6750 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 5150 6850 50  0001 C CNN
	5    5100 6650
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:Rotary_Encoder_Switch-Device SW1
U 1 1 61A6AE8A
P 6850 1200
F 0 "SW1" H 6850 1567 50  0000 C CNN
F 1 "Rotary_Encoder_Switch" H 6850 1476 50  0000 C CNN
F 2 "Rotary_Encoder:RotaryEncoder_Alps_EC11E-Switch_Vertical_H20mm" H 6700 1360 50  0001 C CNN
F 3 "~" H 6850 1460 50  0001 C CNN
	1    6850 1200
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:DIN-5-Connector #J5
U 1 1 61A7322D
P 10150 6000
F 0 "#J5" H 10150 5633 50  0000 C CNN
F 1 "#DIN-5" H 10150 5724 50  0000 C CNN
F 2 "" H 10150 6000 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 10150 6000 50  0001 C CNN
	1    10150 6000
	-1   0    0    1   
$EndComp
$Comp
L MainClock-rescue:Conn_02x05_Odd_Even-Connector_Generic H1
U 1 1 61A75987
P 5900 6650
F 0 "H1" H 5950 7067 50  0000 C CNN
F 1 "IDC 5p" H 5950 6976 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" H 5950 6975 50  0001 C CNN
F 3 "~" H 5900 6650 50  0001 C CNN
	1    5900 6650
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:CP-Device C1
U 1 1 61A7721E
P 6550 6500
F 0 "C1" H 6432 6454 50  0000 R CNN
F 1 "10uF" H 6432 6545 50  0000 R CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P2.00mm" H 6588 6350 50  0001 C CNN
F 3 "~" H 6550 6500 50  0001 C CNN
	1    6550 6500
	-1   0    0    1   
$EndComp
$Comp
L MainClock-rescue:CP-Device C2
U 1 1 61A9352E
P 6550 6800
F 0 "C2" H 6432 6754 50  0000 R CNN
F 1 "10uF" H 6432 6845 50  0000 R CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P2.00mm" H 6588 6650 50  0001 C CNN
F 3 "~" H 6550 6800 50  0001 C CNN
	1    6550 6800
	-1   0    0    1   
$EndComp
Wire Wire Line
	6200 6650 6550 6650
Connection ~ 6550 6650
Wire Wire Line
	6200 6650 5700 6650
Connection ~ 6200 6650
Wire Wire Line
	6200 6550 6200 6650
Wire Wire Line
	6200 6750 6200 6650
Wire Wire Line
	6200 6550 5700 6550
Connection ~ 6200 6550
Wire Wire Line
	6200 6750 5700 6750
Connection ~ 6200 6750
Wire Wire Line
	5700 6550 5700 6650
Connection ~ 5700 6550
Connection ~ 5700 6650
Wire Wire Line
	5700 6750 5700 6650
Connection ~ 5700 6750
Wire Wire Line
	6200 6450 5700 6450
Wire Wire Line
	6200 6850 5700 6850
$Comp
L MainClock-rescue:-12V-power #PWR021
U 1 1 61AB70E1
P 6550 6150
F 0 "#PWR021" H 6550 6250 50  0001 C CNN
F 1 "-12V" H 6565 6323 50  0000 C CNN
F 2 "" H 6550 6150 50  0001 C CNN
F 3 "" H 6550 6150 50  0001 C CNN
	1    6550 6150
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:+12V-power #PWR025
U 1 1 61AB9571
P 6550 7250
F 0 "#PWR025" H 6550 7100 50  0001 C CNN
F 1 "+12V" H 6565 7423 50  0000 C CNN
F 2 "" H 6550 7250 50  0001 C CNN
F 3 "" H 6550 7250 50  0001 C CNN
	1    6550 7250
	-1   0    0    1   
$EndComp
$Comp
L MainClock-rescue:GND-power #PWR023
U 1 1 61ABA3CE
P 5550 6650
F 0 "#PWR023" H 5550 6400 50  0001 C CNN
F 1 "GND" H 5555 6477 50  0000 C CNN
F 2 "" H 5550 6650 50  0001 C CNN
F 3 "" H 5550 6650 50  0001 C CNN
	1    5550 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 6650 5550 6650
$Comp
L MainClock-rescue:1N5819-Diode D6
U 1 1 61ABBFF0
P 6350 7100
F 0 "D6" H 6350 6883 50  0000 C CNN
F 1 "1N5819" H 6350 6974 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 6350 6925 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88525/1n5817.pdf" H 6350 7100 50  0001 C CNN
	1    6350 7100
	-1   0    0    1   
$EndComp
Wire Wire Line
	6550 6950 6550 7100
Wire Wire Line
	6500 7100 6550 7100
Connection ~ 6550 7100
Wire Wire Line
	6550 7100 6550 7250
Wire Wire Line
	6200 7100 6200 6850
Connection ~ 6200 6850
$Comp
L MainClock-rescue:1N5819-Diode D4
U 1 1 61ACA644
P 6350 6250
F 0 "D4" H 6350 6467 50  0000 C CNN
F 1 "1N5819" H 6350 6376 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 6350 6075 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88525/1n5817.pdf" H 6350 6250 50  0001 C CNN
	1    6350 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 6350 6550 6250
Wire Wire Line
	6500 6250 6550 6250
Connection ~ 6550 6250
Wire Wire Line
	6550 6250 6550 6150
Wire Wire Line
	6200 6250 6200 6450
Connection ~ 6200 6450
$Comp
L MainClock-rescue:-12V-power #PWR016
U 1 1 61AE748C
P 5000 6950
F 0 "#PWR016" H 5000 7050 50  0001 C CNN
F 1 "-12V" H 5015 7123 50  0000 C CNN
F 2 "" H 5000 6950 50  0001 C CNN
F 3 "" H 5000 6950 50  0001 C CNN
	1    5000 6950
	-1   0    0    1   
$EndComp
$Comp
L MainClock-rescue:+12V-power #PWR012
U 1 1 61AE82BD
P 5000 6350
F 0 "#PWR012" H 5000 6200 50  0001 C CNN
F 1 "+12V" H 5015 6523 50  0000 C CNN
F 2 "" H 5000 6350 50  0001 C CNN
F 3 "" H 5000 6350 50  0001 C CNN
	1    5000 6350
	1    0    0    -1  
$EndComp
Text Label 8900 1400 2    50   ~ 0
MIDI-OUT
Wire Wire Line
	9100 1400 8900 1400
$Comp
L MainClock-rescue:+12V-power #PWR01
U 1 1 61B08F16
P 9500 1100
F 0 "#PWR01" H 9500 950 50  0001 C CNN
F 1 "+12V" H 9515 1273 50  0000 C CNN
F 2 "" H 9500 1100 50  0001 C CNN
F 3 "" H 9500 1100 50  0001 C CNN
	1    9500 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 1100 9500 1200
$Comp
L MainClock-rescue:GND-power #PWR010
U 1 1 61B0E3CF
P 10100 3200
F 0 "#PWR010" H 10100 2950 50  0001 C CNN
F 1 "GND" H 10105 3027 50  0000 C CNN
F 2 "" H 10100 3200 50  0001 C CNN
F 3 "" H 10100 3200 50  0001 C CNN
	1    10100 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 3200 10100 3200
Text Label 8900 2000 2    50   ~ 0
PWM-1
Text Label 8900 2100 2    50   ~ 0
PWM-2
Text Label 8900 2200 2    50   ~ 0
PWM-3
Text Label 8900 2300 2    50   ~ 0
PWM-4
Wire Wire Line
	9100 2000 8900 2000
Wire Wire Line
	9100 2100 8900 2100
Wire Wire Line
	9100 2200 8900 2200
Text Label 10300 2400 0    50   ~ 0
ENC-A
Text Label 10300 2300 0    50   ~ 0
ENC-B
Text Label 10300 2200 0    50   ~ 0
ENC-SW
Wire Wire Line
	10100 2200 10300 2200
Wire Wire Line
	10100 2300 10300 2300
Wire Wire Line
	10100 2400 10300 2400
Text Label 8900 1700 2    50   ~ 0
CLK
Text Label 8900 1800 2    50   ~ 0
IO
Wire Wire Line
	8900 1700 9100 1700
Wire Wire Line
	8900 1800 9100 1800
$Comp
L MainClock-rescue:+5V-power #PWR02
U 1 1 61B27EAF
P 9800 1100
F 0 "#PWR02" H 9800 950 50  0001 C CNN
F 1 "+5V" H 9815 1273 50  0000 C CNN
F 2 "" H 9800 1100 50  0001 C CNN
F 3 "" H 9800 1100 50  0001 C CNN
	1    9800 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 1100 9800 1200
Wire Wire Line
	7150 1300 7250 1300
Text Label 7250 1100 0    50   ~ 0
ENC-SW.f
Wire Wire Line
	7150 1100 7250 1100
Wire Wire Line
	6550 1200 6450 1200
$Comp
L MainClock-rescue:R-Device R3
U 1 1 61B731A4
P 6000 1100
F 0 "R3" V 5900 1100 50  0000 C CNN
F 1 "50k" V 6000 1100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5930 1100 50  0001 C CNN
F 3 "~" H 6000 1100 50  0001 C CNN
	1    6000 1100
	0    1    1    0   
$EndComp
$Comp
L MainClock-rescue:R-Device R5
U 1 1 61B74739
P 6000 1300
F 0 "R5" V 5900 1300 50  0000 C CNN
F 1 "50k" V 6000 1300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5930 1300 50  0001 C CNN
F 3 "~" H 6000 1300 50  0001 C CNN
	1    6000 1300
	0    -1   1    0   
$EndComp
Text Label 6300 1000 1    50   ~ 0
ENC-A.f
Text Label 6300 1400 3    50   ~ 0
ENC-B.f
Wire Wire Line
	5850 1300 5750 1300
Wire Wire Line
	5750 1300 5750 1100
Wire Wire Line
	5850 1100 5750 1100
Connection ~ 5750 1100
Wire Wire Line
	5750 1100 5750 1000
Wire Wire Line
	6550 1100 6300 1100
Wire Wire Line
	6550 1300 6300 1300
Wire Wire Line
	6300 1400 6300 1300
Connection ~ 6300 1300
Wire Wire Line
	6300 1300 6150 1300
Wire Wire Line
	6300 1000 6300 1100
Connection ~ 6300 1100
Wire Wire Line
	6300 1100 6150 1100
Text Label 2100 1000 1    50   ~ 0
PWM-1
$Comp
L MainClock-rescue:LED-Device D1
U 1 1 61BC6A95
P 1500 1100
F 0 "D1" H 1493 1317 50  0000 C CNN
F 1 "LED" H 1493 1226 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 1500 1100 50  0001 C CNN
F 3 "~" H 1500 1100 50  0001 C CNN
	1    1500 1100
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:R-Device R1
U 1 1 61BC98B8
P 1100 1100
F 0 "R1" V 1000 1100 50  0000 C CNN
F 1 "10k" V 1100 1100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1030 1100 50  0001 C CNN
F 3 "~" H 1100 1100 50  0001 C CNN
	1    1100 1100
	0    1    1    0   
$EndComp
Wire Wire Line
	950  1100 850  1100
Wire Wire Line
	1350 1100 1250 1100
$Comp
L MainClock-rescue:TL074-Amplifier_Operational U1
U 1 1 61A63771
P 2500 1200
F 0 "U1" H 2500 1567 50  0000 C CNN
F 1 "TL074" H 2500 1476 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm_Socket_LongPads" H 2450 1300 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/tl071.pdf" H 2550 1400 50  0001 C CNN
	1    2500 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1100 2100 1100
Wire Wire Line
	2100 1000 2100 1100
Connection ~ 2100 1100
Wire Wire Line
	2100 1100 2200 1100
$Comp
L MainClock-rescue:R-Device R4
U 1 1 61BD6B0A
P 2500 1550
F 0 "R4" V 2400 1550 50  0000 C CNN
F 1 "10k" V 2500 1550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2430 1550 50  0001 C CNN
F 3 "~" H 2500 1550 50  0001 C CNN
	1    2500 1550
	0    1    1    0   
$EndComp
$Comp
L MainClock-rescue:R-Device R6
U 1 1 61BD9701
P 2100 1800
F 0 "R6" V 2000 1800 50  0000 C CNN
F 1 "10k" V 2100 1800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2030 1800 50  0001 C CNN
F 3 "~" H 2100 1800 50  0001 C CNN
	1    2100 1800
	-1   0    0    1   
$EndComp
$Comp
L MainClock-rescue:GND-power #PWR08
U 1 1 61BDA2A0
P 2100 2050
F 0 "#PWR08" H 2100 1800 50  0001 C CNN
F 1 "GND" H 2105 1877 50  0000 C CNN
F 2 "" H 2100 2050 50  0001 C CNN
F 3 "" H 2100 2050 50  0001 C CNN
	1    2100 2050
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:R-Device R2
U 1 1 61BE35E8
P 3450 1200
F 0 "R2" V 3350 1200 50  0000 C CNN
F 1 "1k" V 3450 1200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3380 1200 50  0001 C CNN
F 3 "~" H 3450 1200 50  0001 C CNN
	1    3450 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 1200 3600 1200
Wire Wire Line
	2200 1300 2100 1300
Wire Wire Line
	2100 1300 2100 1550
Wire Wire Line
	2100 1950 2100 2050
Wire Wire Line
	2350 1550 2100 1550
Connection ~ 2100 1550
Wire Wire Line
	2100 1550 2100 1650
Wire Wire Line
	2800 1200 2900 1200
Wire Wire Line
	2900 1200 2900 1550
Wire Wire Line
	2900 1550 2650 1550
Connection ~ 2900 1200
Wire Wire Line
	2900 1200 3000 1200
$Comp
L MainClock-rescue:LED-Device D2
U 1 1 61C7649F
P 1500 2850
F 0 "D2" H 1493 3067 50  0000 C CNN
F 1 "LED" H 1493 2976 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 1500 2850 50  0001 C CNN
F 3 "~" H 1500 2850 50  0001 C CNN
	1    1500 2850
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:LED-Device D3
U 1 1 61C79377
P 1450 4600
F 0 "D3" H 1443 4817 50  0000 C CNN
F 1 "LED" H 1443 4726 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 1450 4600 50  0001 C CNN
F 3 "~" H 1450 4600 50  0001 C CNN
	1    1450 4600
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:LED-Device D5
U 1 1 61C7DE51
P 1400 6350
F 0 "D5" H 1393 6567 50  0000 C CNN
F 1 "LED" H 1393 6476 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 1400 6350 50  0001 C CNN
F 3 "~" H 1400 6350 50  0001 C CNN
	1    1400 6350
	1    0    0    -1  
$EndComp
Text Label 2100 2750 1    50   ~ 0
PWM-2
Text Label 2100 4500 1    50   ~ 0
PWM-3
Text Label 2100 6250 1    50   ~ 0
PWM-4
$Comp
L MainClock-rescue:R-Device R7
U 1 1 61C98835
P 1100 2850
F 0 "R7" V 1000 2850 50  0000 C CNN
F 1 "10k" V 1100 2850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1030 2850 50  0001 C CNN
F 3 "~" H 1100 2850 50  0001 C CNN
	1    1100 2850
	0    1    1    0   
$EndComp
$Comp
L MainClock-rescue:R-Device R11
U 1 1 61C992DD
P 1050 4600
F 0 "R11" V 950 4600 50  0000 C CNN
F 1 "10k" V 1050 4600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 980 4600 50  0001 C CNN
F 3 "~" H 1050 4600 50  0001 C CNN
	1    1050 4600
	0    1    1    0   
$EndComp
$Comp
L MainClock-rescue:R-Device R17
U 1 1 61C9B2AE
P 1000 6350
F 0 "R17" V 900 6350 50  0000 C CNN
F 1 "10k" V 1000 6350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 930 6350 50  0001 C CNN
F 3 "~" H 1000 6350 50  0001 C CNN
	1    1000 6350
	0    1    1    0   
$EndComp
$Comp
L MainClock-rescue:R-Device R9
U 1 1 61CAABD9
P 2500 3300
F 0 "R9" V 2400 3300 50  0000 C CNN
F 1 "10k" V 2500 3300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2430 3300 50  0001 C CNN
F 3 "~" H 2500 3300 50  0001 C CNN
	1    2500 3300
	0    1    1    0   
$EndComp
$Comp
L MainClock-rescue:R-Device R14
U 1 1 61CACF95
P 2500 5050
F 0 "R14" V 2400 5050 50  0000 C CNN
F 1 "10k" V 2500 5050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2430 5050 50  0001 C CNN
F 3 "~" H 2500 5050 50  0001 C CNN
	1    2500 5050
	0    1    1    0   
$EndComp
$Comp
L MainClock-rescue:R-Device R19
U 1 1 61CAEBE2
P 2500 6800
F 0 "R19" V 2400 6800 50  0000 C CNN
F 1 "10k" V 2500 6800 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2430 6800 50  0001 C CNN
F 3 "~" H 2500 6800 50  0001 C CNN
	1    2500 6800
	0    1    1    0   
$EndComp
$Comp
L MainClock-rescue:R-Device R10
U 1 1 61CB0BF6
P 2100 3550
F 0 "R10" V 2000 3550 50  0000 C CNN
F 1 "10k" V 2100 3550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2030 3550 50  0001 C CNN
F 3 "~" H 2100 3550 50  0001 C CNN
	1    2100 3550
	-1   0    0    1   
$EndComp
$Comp
L MainClock-rescue:R-Device R16
U 1 1 61CB6164
P 2100 5300
F 0 "R16" V 2000 5300 50  0000 C CNN
F 1 "10k" V 2100 5300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2030 5300 50  0001 C CNN
F 3 "~" H 2100 5300 50  0001 C CNN
	1    2100 5300
	-1   0    0    1   
$EndComp
$Comp
L MainClock-rescue:R-Device R20
U 1 1 61CB86CA
P 2100 7050
F 0 "R20" V 2000 7050 50  0000 C CNN
F 1 "10k" V 2100 7050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2030 7050 50  0001 C CNN
F 3 "~" H 2100 7050 50  0001 C CNN
	1    2100 7050
	-1   0    0    1   
$EndComp
$Comp
L MainClock-rescue:GND-power #PWR027
U 1 1 61CB973A
P 2100 7300
F 0 "#PWR027" H 2100 7050 50  0001 C CNN
F 1 "GND" H 2105 7127 50  0000 C CNN
F 2 "" H 2100 7300 50  0001 C CNN
F 3 "" H 2100 7300 50  0001 C CNN
	1    2100 7300
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:GND-power #PWR022
U 1 1 61CBA718
P 2100 5550
F 0 "#PWR022" H 2100 5300 50  0001 C CNN
F 1 "GND" H 2105 5377 50  0000 C CNN
F 2 "" H 2100 5550 50  0001 C CNN
F 3 "" H 2100 5550 50  0001 C CNN
	1    2100 5550
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:GND-power #PWR014
U 1 1 61CBD0B0
P 2100 3800
F 0 "#PWR014" H 2100 3550 50  0001 C CNN
F 1 "GND" H 2105 3627 50  0000 C CNN
F 2 "" H 2100 3800 50  0001 C CNN
F 3 "" H 2100 3800 50  0001 C CNN
	1    2100 3800
	1    0    0    -1  
$EndComp
$Comp
L MainClock-rescue:R-Device R8
U 1 1 61CC00CC
P 3500 2950
F 0 "R8" V 3400 2950 50  0000 C CNN
F 1 "1k" V 3500 2950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3430 2950 50  0001 C CNN
F 3 "~" H 3500 2950 50  0001 C CNN
	1    3500 2950
	0    1    1    0   
$EndComp
$Comp
L MainClock-rescue:R-Device R12
U 1 1 61CC09E6
P 3500 4700
F 0 "R12" V 3400 4700 50  0000 C CNN
F 1 "1k" V 3500 4700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3430 4700 50  0001 C CNN
F 3 "~" H 3500 4700 50  0001 C CNN
	1    3500 4700
	0    1    1    0   
$EndComp
$Comp
L MainClock-rescue:R-Device R18
U 1 1 61CC3673
P 3550 6450
F 0 "R18" V 3450 6450 50  0000 C CNN
F 1 "1k" V 3550 6450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3480 6450 50  0001 C CNN
F 3 "~" H 3550 6450 50  0001 C CNN
	1    3550 6450
	0    1    1    0   
$EndComp
Wire Wire Line
	950  2850 850  2850
Wire Wire Line
	1250 2850 1350 2850
Wire Wire Line
	2000 2850 2100 2850
Wire Wire Line
	2100 2750 2100 2850
Connection ~ 2100 2850
Wire Wire Line
	2100 2850 2200 2850
Wire Wire Line
	2800 2950 2900 2950
Wire Wire Line
	3650 2950 3750 2950
Wire Wire Line
	2900 2950 2900 3300
Wire Wire Line
	2900 3300 2650 3300
Connection ~ 2900 2950
Wire Wire Line
	2900 2950 3000 2950
Wire Wire Line
	2200 3050 2100 3050
Wire Wire Line
	2100 3050 2100 3300
Wire Wire Line
	2350 3300 2100 3300
Connection ~ 2100 3300
Wire Wire Line
	2100 3300 2100 3400
Wire Wire Line
	2100 3700 2100 3800
Wire Wire Line
	2200 4600 2100 4600
Wire Wire Line
	2100 4500 2100 4600
Connection ~ 2100 4600
Wire Wire Line
	2100 4600 2000 4600
Wire Wire Line
	1300 4600 1200 4600
Wire Wire Line
	900  4600 800  4600
Wire Wire Line
	2800 4700 2900 4700
Wire Wire Line
	3650 4700 3750 4700
Wire Wire Line
	2900 4700 2900 5050
Wire Wire Line
	2900 5050 2650 5050
Connection ~ 2900 4700
Wire Wire Line
	2900 4700 3000 4700
Wire Wire Line
	2200 4800 2100 4800
Wire Wire Line
	2100 4800 2100 5050
Wire Wire Line
	2350 5050 2100 5050
Connection ~ 2100 5050
Wire Wire Line
	2100 5050 2100 5150
Wire Wire Line
	2100 5450 2100 5550
Wire Wire Line
	2200 6350 2100 6350
Wire Wire Line
	2100 6250 2100 6350
Connection ~ 2100 6350
Wire Wire Line
	2100 6350 2000 6350
Wire Wire Line
	1250 6350 1150 6350
Wire Wire Line
	850  6350 750  6350
Wire Wire Line
	2200 6550 2100 6550
Wire Wire Line
	2100 6550 2100 6800
Wire Wire Line
	2350 6800 2100 6800
Connection ~ 2100 6800
Wire Wire Line
	2100 6800 2100 6900
Wire Wire Line
	2800 6450 2900 6450
Wire Wire Line
	2900 6450 2900 6800
Wire Wire Line
	2900 6800 2650 6800
Connection ~ 2900 6450
Wire Wire Line
	2900 6450 3000 6450
Wire Wire Line
	3700 6450 3800 6450
Wire Wire Line
	2100 7200 2100 7300
Wire Wire Line
	9100 2300 8900 2300
Text Label 10550 6100 0    50   ~ 0
MIDI-PIN2
Text Label 9750 6100 2    50   ~ 0
MIDI-PIN4
Wire Wire Line
	9850 6100 9750 6100
Wire Wire Line
	10450 6100 10550 6100
Text Label 10250 6300 0    50   ~ 0
MIDI-PIN3
Wire Wire Line
	10150 6300 10250 6300
Text Label 10150 4750 0    50   ~ 0
MIDI-PIN2
Text Label 10150 4850 0    50   ~ 0
MIDI-PIN3
Text Label 10150 4950 0    50   ~ 0
MIDI-PIN4
$Comp
L MainClock-rescue:Conn_01x03_Male-Connector J6
U 1 1 61F99DFA
P 9600 4850
F 0 "J6" H 9708 5131 50  0000 C CNN
F 1 "Conn_01x03_Male" H 9708 5040 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 9600 4850 50  0001 C CNN
F 3 "~" H 9600 4850 50  0001 C CNN
	1    9600 4850
	1    0    0    -1  
$EndComp
Text Label 10550 4600 0    50   ~ 0
MIDI-OUT
$Comp
L MainClock-rescue:R-Device R13
U 1 1 61FAD022
P 10300 4600
F 0 "R13" V 10200 4600 50  0000 C CNN
F 1 "220R" V 10300 4600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 10230 4600 50  0001 C CNN
F 3 "~" H 10300 4600 50  0001 C CNN
	1    10300 4600
	0    1    1    0   
$EndComp
Wire Wire Line
	10550 4600 10450 4600
Wire Wire Line
	9800 4750 10050 4750
Wire Wire Line
	10150 4600 10050 4600
Wire Wire Line
	10050 4600 10050 4750
Connection ~ 10050 4750
Wire Wire Line
	10050 4750 10150 4750
$Comp
L MainClock-rescue:+5V-power #PWR020
U 1 1 61FC65EC
P 10550 5050
F 0 "#PWR020" H 10550 4900 50  0001 C CNN
F 1 "+5V" V 10565 5178 50  0000 L CNN
F 2 "" H 10550 5050 50  0001 C CNN
F 3 "" H 10550 5050 50  0001 C CNN
	1    10550 5050
	0    1    1    0   
$EndComp
Wire Wire Line
	9800 4950 10050 4950
Wire Wire Line
	10050 4950 10050 5050
Wire Wire Line
	10050 5050 10150 5050
Connection ~ 10050 4950
Wire Wire Line
	10050 4950 10150 4950
$Comp
L MainClock-rescue:R-Device R15
U 1 1 61FD2F6B
P 10300 5050
F 0 "R15" V 10200 5050 50  0000 C CNN
F 1 "220R" V 10300 5050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 10230 5050 50  0001 C CNN
F 3 "~" H 10300 5050 50  0001 C CNN
	1    10300 5050
	0    1    1    0   
$EndComp
Wire Wire Line
	10550 5050 10450 5050
$Comp
L MainClock-rescue:GND-power #PWR019
U 1 1 61FD910C
P 9900 5050
F 0 "#PWR019" H 9900 4800 50  0001 C CNN
F 1 "GND" H 9905 4877 50  0000 C CNN
F 2 "" H 9900 5050 50  0001 C CNN
F 3 "" H 9900 5050 50  0001 C CNN
	1    9900 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 4850 9900 4850
Wire Wire Line
	9900 4850 9900 5050
$Comp
L MainClock-rescue:Conn_01x04_Female-Connector J3
U 1 1 6209F7FA
P 9850 4000
F 0 "J3" H 9742 4285 50  0000 C CNN
F 1 "Conn_01x04_Female" H 9742 4194 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 9850 4000 50  0001 C CNN
F 3 "~" H 9850 4000 50  0001 C CNN
	1    9850 4000
	-1   0    0    -1  
$EndComp
Text Label 10150 3900 0    50   ~ 0
CLK.f
Text Label 10150 4000 0    50   ~ 0
IO.f
Wire Wire Line
	10050 3900 10150 3900
Wire Wire Line
	10050 4000 10150 4000
Wire Wire Line
	10150 4100 10050 4100
Wire Wire Line
	10050 4200 10150 4200
$Comp
L MainClock-rescue:AudioJack2-Connector J1
U 1 1 621994A2
P 3900 1200
F 0 "J1" H 3720 1183 50  0000 R CNN
F 1 "CLOCK 1/8" H 3720 1274 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_QingPu_WQP-PJ398SM_Vertical_CircularHoles" H 3900 1200 50  0001 C CNN
F 3 "~" H 3900 1200 50  0001 C CNN
	1    3900 1200
	-1   0    0    1   
$EndComp
Wire Wire Line
	3700 1300 3600 1300
$Comp
L MainClock-rescue:AudioJack2-Connector J2
U 1 1 621B5224
P 3950 2950
F 0 "J2" H 3770 2933 50  0000 R CNN
F 1 "CLOCK 1/4" H 3770 3024 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_QingPu_WQP-PJ398SM_Vertical_CircularHoles" H 3950 2950 50  0001 C CNN
F 3 "~" H 3950 2950 50  0001 C CNN
	1    3950 2950
	-1   0    0    1   
$EndComp
$Comp
L MainClock-rescue:AudioJack2-Connector J4
U 1 1 621B6173
P 3950 4700
F 0 "J4" H 3770 4683 50  0000 R CNN
F 1 "CLOCK 1/2" H 3770 4774 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_QingPu_WQP-PJ398SM_Vertical_CircularHoles" H 3950 4700 50  0001 C CNN
F 3 "~" H 3950 4700 50  0001 C CNN
	1    3950 4700
	-1   0    0    1   
$EndComp
$Comp
L MainClock-rescue:AudioJack2-Connector J5
U 1 1 621B80A3
P 4000 6450
F 0 "J5" H 3820 6433 50  0000 R CNN
F 1 "CLOCK 1/1" H 3820 6524 50  0000 R CNN
F 2 "Connector_Audio:Jack_3.5mm_QingPu_WQP-PJ398SM_Vertical_CircularHoles" H 4000 6450 50  0001 C CNN
F 3 "~" H 4000 6450 50  0001 C CNN
	1    4000 6450
	-1   0    0    1   
$EndComp
Wire Wire Line
	3750 3050 3650 3050
Wire Wire Line
	3750 4800 3650 4800
Wire Wire Line
	3700 6550 3800 6550
Text Label 10150 4100 0    50   ~ 0
+5v.f
Text Label 10150 4200 0    50   ~ 0
GND.f
Text Label 7250 1300 0    50   ~ 0
GND.f
Text Label 6450 1200 2    50   ~ 0
GND.f
Text Label 5750 1000 1    50   ~ 0
+5v.f
$Comp
L Connector_Generic:Conn_01x07 J7
U 1 1 61B3D7BE
P 6050 4950
F 0 "J7" H 5968 5467 50  0000 C CNN
F 1 "Conn_01x07" H 5968 5376 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x07_P2.54mm_Vertical" H 6050 4950 50  0001 C CNN
F 3 "~" H 6050 4950 50  0001 C CNN
	1    6050 4950
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x07 J8
U 1 1 61B40BBE
P 7000 4950
F 0 "J8" H 6918 5467 50  0000 C CNN
F 1 "Conn_01x07" H 6918 5376 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x07_P2.54mm_Vertical" H 7000 4950 50  0001 C CNN
F 3 "~" H 7000 4950 50  0001 C CNN
	1    7000 4950
	-1   0    0    -1  
$EndComp
Text Label 6350 4650 0    50   ~ 0
GND.f
Text Label 6350 4750 0    50   ~ 0
+5v.f
Text Label 6350 4850 0    50   ~ 0
CLK.f
Text Label 6350 4950 0    50   ~ 0
IO.f
Text Label 6350 5050 0    50   ~ 0
ENC-A.f
Text Label 6350 5150 0    50   ~ 0
ENC-B.f
Text Label 6350 5250 0    50   ~ 0
ENC-SW.f
Wire Wire Line
	6250 4650 6350 4650
Wire Wire Line
	6250 4750 6350 4750
Wire Wire Line
	6250 4850 6350 4850
Wire Wire Line
	6250 4950 6350 4950
Wire Wire Line
	6250 5050 6350 5050
Wire Wire Line
	6250 5150 6350 5150
Wire Wire Line
	6250 5250 6350 5250
$Comp
L MainClock-rescue:+5V-power #PWR0101
U 1 1 61B7F98F
P 7300 4750
F 0 "#PWR0101" H 7300 4600 50  0001 C CNN
F 1 "+5V" V 7315 4878 50  0000 L CNN
F 2 "" H 7300 4750 50  0001 C CNN
F 3 "" H 7300 4750 50  0001 C CNN
	1    7300 4750
	0    1    1    0   
$EndComp
Text Label 7300 4850 0    50   ~ 0
CLK
Text Label 7300 4950 0    50   ~ 0
IO
$Comp
L MainClock-rescue:GND-power #PWR0102
U 1 1 61B81178
P 7700 4650
F 0 "#PWR0102" H 7700 4400 50  0001 C CNN
F 1 "GND" H 7705 4477 50  0000 C CNN
F 2 "" H 7700 4650 50  0001 C CNN
F 3 "" H 7700 4650 50  0001 C CNN
	1    7700 4650
	1    0    0    -1  
$EndComp
Text Label 7300 5050 0    50   ~ 0
ENC-A
Text Label 7300 5150 0    50   ~ 0
ENC-B
Text Label 7300 5250 0    50   ~ 0
ENC-SW
Wire Wire Line
	7200 4650 7700 4650
Wire Wire Line
	7200 4750 7300 4750
Wire Wire Line
	7300 4850 7200 4850
Wire Wire Line
	7300 4950 7200 4950
Wire Wire Line
	7300 5050 7200 5050
Wire Wire Line
	7300 5150 7200 5150
Wire Wire Line
	7300 5250 7200 5250
Text Label 3600 1300 3    50   ~ 0
GND.f
Text Label 3650 3050 3    50   ~ 0
GND.f
Text Label 3700 6550 3    50   ~ 0
GND.f
Text Label 3650 4800 3    50   ~ 0
GND.f
Text Label 850  1100 2    50   ~ 0
GND.f
Text Label 850  2850 2    50   ~ 0
GND.f
Text Label 800  4600 2    50   ~ 0
GND.f
Text Label 750  6350 2    50   ~ 0
GND.f
$Comp
L Connector_Generic:Conn_01x04 J9
U 1 1 61C2995B
P 6050 2650
F 0 "J9" H 5968 2967 50  0000 C CNN
F 1 "Conn_01x04" H 5968 2876 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 6050 2650 50  0001 C CNN
F 3 "~" H 6050 2650 50  0001 C CNN
	1    6050 2650
	-1   0    0    -1  
$EndComp
Text Label 6350 2550 0    50   ~ 0
J1.f
Text Label 6350 2650 0    50   ~ 0
D1.f
Text Label 6350 2750 0    50   ~ 0
J2.f
Text Label 6350 2850 0    50   ~ 0
D2.f
Wire Wire Line
	6250 2550 6350 2550
Wire Wire Line
	6350 2650 6250 2650
Wire Wire Line
	6350 2750 6250 2750
Wire Wire Line
	6350 2850 6250 2850
$Comp
L Connector_Generic:Conn_01x04 J10
U 1 1 61C4A49A
P 6950 2650
F 0 "J10" H 6868 2967 50  0000 C CNN
F 1 "Conn_01x04" H 6868 2876 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6950 2650 50  0001 C CNN
F 3 "~" H 6950 2650 50  0001 C CNN
	1    6950 2650
	-1   0    0    -1  
$EndComp
Text Label 7250 2550 0    50   ~ 0
J1
Text Label 7250 2750 0    50   ~ 0
J2
Text Label 7250 2850 0    50   ~ 0
D2
Wire Wire Line
	7150 2850 7250 2850
Wire Wire Line
	7150 2750 7250 2750
Wire Wire Line
	7250 2650 7150 2650
Wire Wire Line
	7150 2550 7250 2550
Text Label 3000 1200 0    50   ~ 0
J1
Text Label 3000 2950 0    50   ~ 0
J2
Text Label 3300 1200 2    50   ~ 0
J1.f
Text Label 3350 2950 2    50   ~ 0
J2.f
Text Label 1650 1100 0    50   ~ 0
D1.f
Text Label 1650 2850 0    50   ~ 0
D2.f
Text Label 2000 1100 2    50   ~ 0
D1
Text Label 2000 2850 2    50   ~ 0
D2
$Comp
L Connector_Generic:Conn_01x04 J11
U 1 1 61D0ABBE
P 6050 3650
F 0 "J11" H 5968 3967 50  0000 C CNN
F 1 "Conn_01x04" H 5968 3876 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 6050 3650 50  0001 C CNN
F 3 "~" H 6050 3650 50  0001 C CNN
	1    6050 3650
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J12
U 1 1 61D0B897
P 6950 3650
F 0 "J12" H 6868 3967 50  0000 C CNN
F 1 "Conn_01x04" H 6868 3876 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6950 3650 50  0001 C CNN
F 3 "~" H 6950 3650 50  0001 C CNN
	1    6950 3650
	-1   0    0    -1  
$EndComp
Text Label 6350 3750 0    50   ~ 0
J4.f
Text Label 6350 3850 0    50   ~ 0
D3.f
Text Label 6350 3550 0    50   ~ 0
J5.f
Text Label 6350 3650 0    50   ~ 0
D5.f
Text Label 7250 3750 0    50   ~ 0
J4
Text Label 7250 3550 0    50   ~ 0
J5
Text Label 7250 2650 0    50   ~ 0
D1
Text Label 7250 3850 0    50   ~ 0
D3
Text Label 7250 3650 0    50   ~ 0
D5
Wire Wire Line
	7250 3550 7150 3550
Wire Wire Line
	7250 3650 7150 3650
Wire Wire Line
	7250 3750 7150 3750
Wire Wire Line
	7250 3850 7150 3850
Wire Wire Line
	6350 3550 6250 3550
Wire Wire Line
	6350 3650 6250 3650
Wire Wire Line
	6350 3750 6250 3750
Wire Wire Line
	6350 3850 6250 3850
Text Label 1600 4600 0    50   ~ 0
D3.f
Text Label 1550 6350 0    50   ~ 0
D5.f
Text Label 2000 4600 2    50   ~ 0
D3
Text Label 2000 6350 2    50   ~ 0
D5
Text Label 3350 4700 2    50   ~ 0
J4.f
Text Label 3400 6450 2    50   ~ 0
J5.f
Text Label 3000 4700 0    50   ~ 0
J4
Text Label 3000 6450 0    50   ~ 0
J5
$EndSCHEMATC
